![logo](https://demo-pretty-noemie.frama.site/public/img/logo-final.svg)

# NoemieCms
![enter image description here](https://framablog.org/wp-content/uploads/2018/02/pages-framasite-theme-light.gif)

CMS offrant à ses utilisateurs une solution ergonomique, simple et élégante pour construire en un rien de temps des sites vitrines responsives au design moderne.


La construction de votre site consistera à agencer à votre convenance des modules variés, d'éditer leurs contenus, et de personnaliser votre site en choisisant les polices de caractère, la mise en forme du texte, ainsi que les couleurs d'affichage


----------

## Essayez la DEMO !


## **[ https://demo-pretty-noemie.frama.site](https://demo-pretty-noemie.frama.site/login)**
Connectez vous avec :

    login : pretty
    mdp : 12345678

(Lorsque plusieurs personnes sont connectées en même temps au même site avec le même compte, il croit qu'il y a des csrf et il vous renvoie des erreurs)

## Grâce à ses modules variés, il conviendra parfaitement à de nombreuses utilisations :

 - Réalisation d'un CV en ligne
 - Portfolio d'artiste ou de créateur
 - Vitrine de votre association
 - Présentation de votre travail d'artisan
 - Page d'accueil d'un festival
 - Et tout ce que votre créativité en fera
 

## Utilisé par Framasoft pour construire les Pages Framasite

Vous pouvez créer gratuitement et librement votre framasite ici : 
[https://frama.site/](https://frama.site/)


----------

## Soutenir
Pour soutenir le créateur de ce projet libre et open source, vous pouvez faire une donation sur Patreon : 

 - [https://www.patreon.com/robinbanquo](https://www.patreon.com/robinbanquo)


---------- 

## Derniers ajouts

 - Module réseaux sociaux
 - Menu en version mobile
 - Module lecteur audio (soundcloud) pour les personnes souhaitant faire le site de leur groupe de musique
 - Module avec lecteur Vidéo et vidéo d'arrière plan
 - Module Code avec coloration syntaxique, pour realiser des tutos
 - Redesign des fenetres
 - Redesign de la fenetre de choix de modules (grace à la contribution d'un designer)


## Lancer PrettyNoemie en local

télécharger les fichier en zip ou clonez le repository avec :

    git clone https://framagit.org/framasoft/PrettyNoemieCMS

-puis à l'interieur du dossier, lancez la commande d'installation avec Composer ( [pour installer Composer, c'est ici](https://getcomposer.org/)  ) : 
  

    composer install

lancez enfin le serveur localintégré à php (php7 voire un poil moins ) avec : 

    php -S localhost:8000

Subtilité : le fichier *app/config/config.ini* à une option qui est a **true** en production et à **""** en devellopement.
vérifiez que vous avez bien : 

    [globals]
    /autres options/
    /autres options/
    /autres options/
    isMediaPathModified =
    
Le compte d'admin en localhost :

    login: admin
    mdp: password

## Contribuer

Les Merges requests sont les bienvenues, et le bug report et les suggestions sont trés appréciées

Un chat de discussion publique est ouvert autour du projet pour venir discuter (ya pas forcement grand monde dessus, mais j'ai des notifs quant il y a des messages, donc n'hesitez pas)

[https://riot.im/app/#/room/!wPhaCatDceeQqOQqdM:matrix.org](https://riot.im/app/#/room/!wPhaCatDceeQqOQqdM:matrix.org)
(préferez les issues pour les bugs reports et autres discussions plus formelles autour du projet)

pour savoir comment faire, voici un tuto sur l'utilisation de git et gihub pour contribuer :

[contribuer à des projetsopen source](https://openclassrooms.com/courses/gerer-son-code-avec-git-et-github/contribuer-a-des-projets-open-source)


----------


## Licence

GNU AFFERO GENERAL PUBLIC LICENSE    Version 3, 19 November 2007


